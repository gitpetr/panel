function toolToggle(tool) {
  document.querySelector(tool).addEventListener('mouseenter', function (e) {
    let el = document.querySelector(tool + '+ div ')
    el.querySelector('.background-pic').classList.remove('background-pic-hidden')
    el.querySelector('.background-pic').classList.add('background-pic-visibility')
  })

  document.querySelector(tool).addEventListener('mouseleave', function (e) {
    let el = document.querySelector(tool + '+ div ')
    el.querySelector('.background-pic').classList.remove('background-pic-visibility')
    el.querySelector('.background-pic').classList.add('background-pic-hidden')
  })
}

toolToggle('.d25')
toolToggle('.d31')
toolToggle('.d32')
toolToggle('.d41')
toolToggle('.d43')
toolToggle('.d24')
